#!/bin/bash

# VARIABLES

source config.conf

#SET YOUR FAVORITE TERMINAL EMULATOR HERE
terminal=xterm
clear

# Introduction


if [ $firstStart = 'true' ]; then
    
    echo "It seems like this is the first time you run the program, Do you want to Setup your config file? y/n ";
    read input;
    
    if [ $input = 'y' ]; then
	editor config.conf;
	sed -i 's/firstStart="true"/firstStart="false"/g' config.conf;
	clear
    else
	echo "Ok, you can modify it later";
	sed -i 's/firstStart="true"/firstStart="false"/g' config.conf;
	sleep 2;
	clear;
    fi 
fi


echo -e "$orange $greeting back $user! I am your helper.$NC"
echo "Today is $day"
echo -e "$green $weather"

# Export and update the calendar

$updateCal

echo -e "\n"

# Mouse art
echo -e "$green   __QQ$NC"
echo -e "$green  (_)_'> $NC"
echo -e "$green _)$NC"
echo -e "\n"

# General Info
echo -e "Your Bash shell version is: $BASH_VERSION. Enjoy!\n"
echo "##################################"
echo -e $red "STORAGE STATS" $NC
df -h
echo -e "##################################\n"
echo -e $red "TO-DO" $NC
calcurse -t
echo -e "##################################\n"


# The function commands starts the commands 

function commands {
echo -e $green "What would you like to do?" $NC
echo -e $black "MAIN MENU" $NC

select option in $program1 $program2 $program3 $program4 $program5 "Restart" "system_options" $program6 $program7 "Tear_Free-AMD" "Exit"; do 
    case $option in
        $program1 ) $program1Path; break;;
	$program2 ) $program2Path &> /dev/null & disown; break;;
	$program3 ) "${program3Path[@]}" &> /dev/null & disown; break;; 
        $program4 ) $program4Path &> /dev/null & disown; break;; 
        $program5 ) $program5Path &> /dev/null & disown; break;; 
	Restart ) clear; break;;
	system_options ) systemcmd; break;;
        $program6 ) $terminal -e "sudo $program6Path; bash" & disown; break;;
	$program7 ) $terminal -e "$program7Path; bash" & disown; break;;
	Tear_Free-AMD ) xterm -e "sudo editor /etc/X11/xorg.conf; bash" & disown; break;; 
	Exit ) kill $PPID; exit; break;;
    esac
done
}

function systemcmd {
echo -e $green "What do you want to do?" $NC
echo -e $black "SYSTEM OPTIONS" $NC 
select option in "back" "update" "upgrade" "night" "day" "shutdown" "restart" "suspend" "importCal"; do
    case $option in
        back ) echo " "; break;;
        update ) if [ $packageManager = 'apt' ]; then sudo $packageManager update; else sudo $packageManager -Syy; fi; break;;                                                                
        upgrade ) if [ $packageManager = 'apt' ]; then sudo $packageManager upgrade; else sudo $packageManager -Syu; fi; break;;
	night ) redshift -O 4500 &> /dev/null ; break;;
	day ) redshift -O 6500 &> /dev/null ; break;;
	shutdown ) systemctl poweroff; break;;
	restart ) systemctl reboot; break;;
	suspend ) systemctl suspend; break;;
	importCal ) $importCal; break;;
	Exit ) exit; break;;
    esac
done
}

commands

while commands
do
  commands
		
done
