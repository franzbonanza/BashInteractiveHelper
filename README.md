# BashInteractiveHelper

Are you a WM user like me? Cool! then you could find this little program useful. I wrote this program to deal with frequently used commands and programs and I use this program everyday. I simply cleaned the code a little and published it but I'll maybe improve this little program in the future. Enjoy!

![Screenshot](screenshot.jpg)

Programs Suggested to use it without issues:
- calcurse
- redshift

## License
The software is released under the GNU General Public License (GPL) which can be found in the file [`LICENSE`](/LICENSE) in the same directory as this file.

---
